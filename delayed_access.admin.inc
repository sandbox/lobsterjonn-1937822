<?php

/**
 * Implements hook_admin()
 */
function delayed_access_admin() {
  $form = array();
  
  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
  );
  
  $form['settings']['delayed_access_test'] = array(
    '#type' => 'textfield',
    '#title' => t('Delayed access'),
    '#default_value' => variable_get('delayed_access_test', ''),
    '#description' => t("Just a setting."),
    '#required' => FALSE,
  );
  
  return system_settings_form($form);
}

/**
 * Page with all delays
 */
function delayed_access_delays_table() {
  $delays = delayed_access_load_all();
  $roles = user_roles();
  $types = node_type_get_types();
  $status = array(
    t('inactive'),
    t('active'),
  );
  
  $header = array(
    array(
      'data' => t('Role'),
      'field' => 'd.rid',
      'sort' => 'desc',
    ),
    array(
      'data' => t('Content type'),
      'field' => 'd.type',
    ),
    array(
      'data' => t('Delay'),
      'field' => 'd.delay',
    ),
    array(
      'data' => t('Status'),
      'field' => 'd.status',
    ),
    array(
      'data' => t('Operations'),
      'colspan' => 2,
    ),
  );
  
  $options = array();
  foreach ($delays as $did => $delay) {
    $options[$did] = array(
      $roles[$delay->rid],
      $types[$delay->type]->name,
      format_interval($delay->delay),
      $status[$delay->status],
      array(
        'data' => array(
          '#type' => 'link',
          '#title' => t('edit'),
          '#href' => "admin/config/komunikado/delayed_access/delays/$did/edit",
          '#options' => array(
              'query' => "",
          ),
        ),
      ),
      array(
        'data' => array(
          '#type' => 'link',
          '#title' => t('delete'),
          '#href' => "admin/config/komunikado/delayed_access/delays/$did/delete",
          '#options' => array(
              'query' => "",
          ),
        ),
      ),
    );
  }
  
  return array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $options,
    '#empty' => t('No delays. <a href="@link">Add one</a>.', array('@link' => url( 'admin/config/komunikado/delayed_access/delays/add'))),
  );
}

/**
 * Add/edit form
 */
function delayed_access_delay_form($form, &$form_state, $delay = NULL) {
  $form = array();
  
  $form['did'] = array(
    '#type' => 'value',
    '#value' => isset($delay->did ) ? $delay->did : NULL,
  );
  
  $form['rid'] = array(
    '#type' => 'select',
    '#title' => t('Role'),
    '#options' => user_roles(),
    '#default_value' => isset($delay->rid ) ? $delay->rid : NULL,
  );
  
  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Content type'),
    '#options' => node_type_get_names(),
    '#default_value' => isset($delay->type ) ? $delay->type : NULL,
  );
  
  $form['delay'] = array(
    '#type' => 'textfield',
    '#title' => t('Delay'),
    '#default_value' => isset($delay->delay ) ? format_interval( $delay->delay ) : NULL,
    '#description' => t('A date/time offset. Examples: 1 day, 2 hours, 1 week 2 days 4 hours 2 seconds, next Thursday etc.'),
  );
  
  $form['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Active'),
    '#default_value' => 1,
  );
  
  $form['actions'] = array(
    '#type' => 'actions'
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  
  return $form;
}

/**
 * Implements hook_form_validate()
 */
function delayed_access_delay_form_validate($form, &$form_state) {
  // Delay must be valid
  if (strtotime($form_state['values']['delay']) === FALSE) {
    form_set_error('delay', t('Date/time string could not be converted to a unix timestamp.'));
  }
}

/**
 * Implements hook_form_submit();
 */
function delayed_access_delay_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  
  $fields = array(
    'did' => $values['did'],
    'rid' => $values['rid'],
    'type' => $values['type'],
    'delay' => strtotime($values['delay']) - time(),
    'status' => $values['status'],
  );
  
  if (is_numeric( $values['did'] )) {
    db_update('delayed_access_delays')->fields($fields)
      ->condition('did', $values['did'])
      ->execute();
  } else {
    db_insert('delayed_access_delays')->fields($fields)
      ->execute();
  }
  drupal_goto( isset($_GET['destination'] ) ? $_GET['destination'] : "admin/config/komunikado/delayed_access/delays" );
}

/**
 * Delete form confirm
 */
function delayed_access_delete_confirm($form, &$form_state, $delay) {
  $form['did'] = array(
    '#type' => 'value',
    '#value' => $delay->did,
  );
  
  return confirm_form($form, t('Are you sure you want to delete this?'), isset($_GET['destination'] ) ? $_GET['destination'] : "admin/config/komunikado/delayed_access/delays", t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * Implements hook_form_submit();
 */
function delayed_access_delete_confirm_submit($form, &$form_state) {
  $form_values = $form_state['values'];
  
  if ($form_state['values']['confirm']) {
    delayed_access_delete($form_values['did']);
    drupal_set_message("Delay deleted.");
  }
  drupal_goto();
}
